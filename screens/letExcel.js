





import XLSX from 'xlsx';

import React, { Component,useState } from 'react';
import { PermissionsAndroid,StyleSheet, Text, View, Button, Alert, Image, ScrollView, TouchableWithoutFeedback,Platform} from 'react-native';
import { Table, Row, Rows, TableWrapper } from 'react-native-table-component';

import { Appbar, RadioButton,Divider } from 'react-native-paper';
import firestore from '@react-native-firebase/firestore';
import { ListItem,Badge} from 'react-native-elements'

// react-native-fs
import {readFile,writeFile, DocumentDirectoryPath,DownloadDirectoryPath } from 'react-native-fs';


const DDP = DownloadDirectoryPath + "/";

var SL=[];
var LL=[];
var LR=[];
var SL0=[];
var LL0=[];
var LR0=[];

var SL1=[];
var LL1=[];
var LR1=[];
var SL01=[];
var LL01=[];
var LR01=[];

var SL2=[];
var LL2=[];
var LR2=[];
var SL02=[];
var LL02=[];
var LR02=[];

var AASL=[];
var AALL=[];
var AALR=[];
var AASL0=[];
var AALL0=[];
var AALR0=[];


var LAS;
var II;


var picStrat=1;
var ii=[];

const input = res => res;
const output = str => str;
var nameUser;

export default class LetExcel extends Component {
	constructor({props,route}) {
        SL=[];
        LL=[];
        LR=[];
        
        SL1=[];
        LL1=[];
        LR1=[];
        
        SL2=[];
        LL2=[];
        LR2=[];
        
        AASL=[];
        AALL=[];
        AALR=[];


		super(props);
		this.dbRef = firestore();
		this.state = {isLoading: false,
            userArr:[],
	

			data: [["1","2","3"],
      [1,2,3],
      [1,2,3],
      [1,2,3],
      [1,2,3],
      [1,2,3],],

		};
		
		this.exportFile = this.exportFile.bind(this);

		const {itemId} = route.params;
        console.log(itemId);
        nameUser=itemId;
	};

	


    componentDidMount(){  
        console.log('this one call');
        this.unsubscribe = this.dbRef.collection(nameUser).orderBy("timestamp").onSnapshot(this.getCollection);
      }

      getCollection=(querySnapshot)=>{
        const userArr=[];
        querySnapshot.forEach((res) => {
            const {    
                LS1,
                LSTF1,
                LM1,
        
                LSF2,
                LSTF2,
                LM2,
        
                LSF3,
                LSTF3,
                LM3,
                
                LSFA,
                LSTFA,
                LMA,
                
                LtimeNeckFlex} =res.data();
            userArr.push({
                key: res.id,
                res,
               
                LS1,
                LSTF1,
                LM1,
        
                LSF2,
                LSTF2,
                LM2,
        
                LSF3,
                LSTF3,
                LM3,
                
                LSFA,
                LSTFA,
                LMA,
                
                LtimeNeckFlex
                
            })
            this.setState({
                userArr,
                isLoading:false
            })
        });
        calcuLet();
        }


requestRunTimePermission=()=>{
  var that =this;
  async function externalStoragePermission(){
try{
  const granted = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
    {
      title :"External Storage Write Permission",
      message :"App needs access to Storage data.",
    }
  );
  if(granted === PermissionsAndroid.RESULTS.GRANTED){
    that.exportFile();
	start=[];
  }else{
    alert('WRITE_EXTERNAL_STORAGE permission denied');
  }
}catch (err){
  Alert,alert('Write permission err',err);
}
}

if(Platform.OS === 'android'){
  externalStoragePermission();
}else{
  this.exportFile();
}
}
exportFile() {
		var date = new Date().getDate(); //To get the Current Date
		var month = new Date().getMonth(); //To get the Current Month
		var year = new Date().getFullYear(); //To get the Current Year
		var hours = new Date().getHours(); //To get the Current Hours
		var min = new Date().getMinutes(); //To get the Current Minutes
		var sec = new Date().getSeconds(); //To get the Current Seconds
		var  Alltime=date+'|'+month+'|'+year;
	
	 var data=[];
   var dataL1=[];
   var dataL2=[];
   var dataL3=[];
const K=0;
SL1=[];
LL1=[];
LR1=[];

SL2=[];
LL2=[];
LR2=[];


for (let index = 0; index < SL.length; index++) 
      {
        if(typeof(SL[index]) !== 'undefined' && SL[index] != null ||typeof(SL1[index]) !== 'undefined' && SL1[index] != null||typeof(SL2[index]) !== 'undefined' && SL2[index] != null||typeof(AASL[index]) !== 'undefined' && AASL[index] != null){            
          data.push(
            {"No":"1","Start":SL[index],"Left":LL[index],"Right":LR[index]},
            {"No":"2","Start":SL1[index],"Left":LL1[index],"Right":LR1[index]},
            {"No":"3","Start":SL2[index],"Left":LL2[index],"Right":LR2[index]},
            {"No":"avg.","Start":AASL[index],"Left":AALL[index],"Right":AALR[index]},
            {"":""}
            );
        }
      }
      console.log(data);


		const ws = XLSX.utils.json_to_sheet(data);

		/* build new workbook */
		const wb = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(wb, ws, "SheetJS");

		/* write file */
		
		const G="LetSheet"+Alltime+".xlsx";
		
		const wbout = XLSX.write(wb, {type:'binary', bookType:"xlsx"});
		const file = DDP + G ;

		writeFile(file, output(wbout), 'ascii').then((res) =>{
				Alert.alert("exportFile success", "Exported to " + file);
		}).catch((err) => { Alert.alert("exportFile Error", "Error " + err.message); });

	};
	




	render() { return (


<ScrollView>

            {
				
				this.state.userArr.map((item,i)=>{
	
				  ii.push(picStrat);
				  II=ii.length;
				  LAS=ii.length-(i+1);

				  SL0.push(item.LS1);
				  LL0.push(item.LSTF1);
				  LR0.push(item.LM1);

				  SL01.push(item.LSF2);
				  LL01.push(item.LSTF2);
				  LR01.push(item.LM2);

				  SL02.push(item.LSF3);
				  LL02.push(item.LSTF3);
				  LR02.push(item.LM3);

				  AASL0.push(item.LSFA);
				  AALL0.push(item.LSTFA);
				  AALR0.push(item.LMA);
				 })
				 
			}


<Text style={styles.welcome}>EXPORT DATA TO EXCEL FILE</Text>

<View style={{marginTop:10}}>
<Button onPress={()=>{this.requestRunTimePermission()}} 
            title="Export data" color="#008080" />
</View>
<Text>Letterral</Text>

</ScrollView>
	); };
};

const styles = StyleSheet.create({
	container: { flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#F5FCFF' },
	welcome: { fontSize: 20, textAlign: 'center', margin: 10 },
	instructions: { textAlign: 'center', color: '#333333', marginBottom: 5 },
	thead: { height: 40, backgroundColor: '#f1f8ff' },
	tr: { height: 30 },
	text: { marginLeft: 5 },
	table: { width: "100%" }
});

function calcuLet(){
	for (let index = LAS; index < II; index++) {
		SL.push(SL0[index]);	
		LL.push(LL0[index]);
		LR.push(LR0[index]);
	}
	for (let index = LAS; index < II; index++) {
		SL1.push(SL01[index]);	
		LL1.push(LL01[index]);
		LR1.push(LR01[index]);
	}
	for (let index = LAS; index < II; index++) {
		SL2.push(SL02[index]);	
		LL2.push(LL02[index]);
		LR2.push(LR02[index]);
	}
	for (let index = LAS; index < II; index++) {
		AASL.push(AASL0[index]);	
		AALL.push(AALL0[index]);
		AALR.push(AALR0[index]);
	}
  for (let index = 0; index < SL.length; index++) {
if(typeof(SL[index]) !== 'undefined' && SL[index] != null){
  console.log(SL[index]); 
}

 
  }
  
}

