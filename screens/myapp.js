import React from "react";
import { StyleSheet,ScrollView,ActivityIndicator,View,Text, ImageBackgroundBase} from 'react-native'
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { ThemeProvider,Button, Input, Image } from 'react-native-elements'
import  Icon  from 'react-native-vector-icons/FontAwesome'
import { BackgroundImage } from 'react-native-elements/dist/config'


function HomeScreen({navigation}){
    return(
      // <View>
      //   <Text> HomeScreen </Text>
      //   <Button title="Go to detail" onPress={()=> navigation.navigate("Details") }/>
        <ThemeProvider theme={theme}>
                <ScrollView style={styles.container}>
                    <Image
                    source={{uri:'https://img.pngio.com/png-logo-design-transparent-logo-designpng-images-pluspng-png-logo-design-734_634.png'}}
                    style={{width:150,height:150}}
                    containerStyle={{marginLeft:'auto',marginRight:'auto',marginTop: 40,marginBottom:30}}
                    
                    />
                  <View><Text>    Your Usernsme</Text></View>  
                    <Input 
                        leftIcon={
                            <Icon
                                name='user-o'
                                size={20}
                                color='#f08080'
                            />
                        }
                        placeholder={'  Simple User'}
                      
                    />
                    <View><Text>     Password</Text></View>  
                    <Input 
                        leftIcon={
                            <Icon
                                name='key'
                                size={20}
                                color='#f08080'
                            />
                        }
                        placeholder={'  Password'}
                  
                    />
            
                   <Button
                    icon={
                        <Icon 
                        name='check'
                        size={15}
                        color='white'
                        />
                    }
                        title=' Login'
                        onPress={() => navigation.navigate("Dashboard")}
                        containerStyle={{
                          marginTop: 30,
                          marginLeft: 20,
                          marginRight: 20
                      }}
                        buttonStyle={{
                            backgroundColor: "green"
                        }}
                   
                    />

                    <Button
                    icon={
                        <Icon 
                        name='user'
                        size={15}
                        color='white'
                        />
                    }
                        title=' Sign In'
                        onPress={() => navigation.navigate("Home2")}
                        containerStyle={{
                            marginTop: 10,
                            marginLeft: 20,
                            marginRight: 20
                        }}
                        
                    />


                    <Button
                    icon={
                        <Icon 
                        name='user'
                        size={15}
                        color='white'
                        />
                    }
                        title=' Sign In'
                        onPress={() => navigation.navigate("AccelerometerPage")}
                        containerStyle={{
                            marginTop: 10,
                            marginLeft: 20,
                            marginRight: 20
                        }}
                        
                    />      

            
                </ScrollView>   
            </ThemeProvider>
      
      
      
      //</View>
    )
  }
  const theme = {
    Button:{
        raised: true
    }
}

const styles =StyleSheet.create({
    container:{
        flex:1,
        padding:20
    },
    preloader:{
        position:'absolute',
        top:0,
        left:0,
        right:0,
        bottom:0,
        alignItems:'center',
        justifyContent:'center'
    }
})
export default HomeScreen;





