//new versuion
import React, {Component,useState} from 'react'
import { StyleSheet,ScrollView,ActivityIndicator,View ,Alert,Text, ImageBackgroundBase} from 'react-native'
import { BackgroundImage } from 'react-native-elements/dist/config'

import  Icon  from 'react-native-vector-icons/FontAwesome'
import { ThemeProvider,Button, Input, Image } from 'react-native-elements'
import firestore from '@react-native-firebase/firestore';
import { ListItem,Badge} from 'react-native-elements'

var IDconfig;
var TureID=' ';

var picPassIN;
var demopass2;
var nameUser;

class ArmPage extends Component{
    
    constructor({route,navigation}){
       

        super();
        this.dbRef = firestore();
        this.state = {
            isLoading: false,
            userArr:[]
        }
        const {itemId} = route.params;
        console.log(itemId);
        nameUser=itemId;
    }

    componentDidMount(){  
        console.log('this one call');
        this.unsubscribe = this.dbRef.collection(nameUser).orderBy("timestamp").onSnapshot(this.getCollection);
      }

getCollection=(querySnapshot)=>{
const userArr=[];
querySnapshot.forEach((res) => {
    const {timeArm,StartArm,StopArm,MoveArm} =res.data();
    userArr.push({
        key: res.id,
        res,
        timeArm,StartArm,StopArm,MoveArm
    })
    this.setState({
        userArr,
        isLoading:false
    })
});
}


render(){

    if(this.state.isLoading){
            return(
                <View style={styles.preloader}>
                    <ActivityIndicator size='large' color="#9E9E9E" />
                </View>
            )
    }
return(


<ScrollView>
    <View style={{marginBottom:10 ,marginTop:10, marginLeft:15}}>
        <Text style={{fontSize:16,fontWeight: "bold"}}>รายละเอียดการวัดของคุณ {nameUser}</Text>
    </View>
                    {
                      this.state.userArr.map((item,i)=>{
                        demopass2=item.name;
                        if(item.StartArm){
                          return(
                              <ListItem key={i}
                              bottomDivider
                              roundAvatar={true}
                              >
                                  
                                  <ListItem.Content>
                                      <ListItem.Title>{item.MoveArm}</ListItem.Title>   
                                      <ListItem.Subtitle>{item.StartArm}</ListItem.Subtitle>
                                      <ListItem.Subtitle>{item.StopArm}</ListItem.Subtitle> 
                                      <ListItem.Subtitle>{item.timeArm}</ListItem.Subtitle> 
                                  </ListItem.Content>
                              </ListItem>
                              
                           )
                          }
                       })
                  }
</ScrollView>
    )
}
}

const theme = {
    Button:{
        raised: true
    }
}


const styles =StyleSheet.create({
    container:{
        flex:1,
        padding:35
    },
    preloader:{
        position:'absolute',
        top:0,
        left:0,
        right:0,
        bottom:0,
        alignItems:'center',
        justifyContent:'center'
    }
})

export default ArmPage;
