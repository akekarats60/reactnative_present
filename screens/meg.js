import React, {Component,useState, useEffect} from "react";
import {Text, View, Image, Dimensions,StyleSheet,ActivityIndicator,Button,Alert,SafeAreaView,ScrollView} from "react-native";
import {Grid, Col, Row} from "react-native-easy-grid";
import {magnetometer, SensorTypes, setUpdateIntervalForType} from "react-native-sensors";
import LPF from "lpf";
import  Icon  from 'react-native-vector-icons/FontAwesome'
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import { ThemeProvider} from 'react-native-elements'
import firestore from '@react-native-firebase/firestore';
var conAngle=0;



const {height, width} = Dimensions.get("window");
var _picMS=0;
var _picME=0;
var st_button=false;
var stst="START";
var colorBT="#000080";
var valueDG;
var R;

var MOVE;
var MET=0;
var CA=0;


export default class MagnetometerPage extends Component {

  constructor({route,navigation}) {
    super();

    this.state = {
      magnetometer: "0",
    };
    LPF.init([]);
    LPF.smoothing = 0.4;
    const {itemId} = route.params;
    R=itemId;
  console.log(itemId);
  }


  storeUser() {
    var date = new Date().getDate(); //To get the Current Date
    var month = new Date().getMonth(); //To get the Current Month
    var year = new Date().getFullYear(); //To get the Current Year
    var hours = new Date().getHours(); //To get the Current Hours
    var min = new Date().getMinutes(); //To get the Current Minutes
    var sec = new Date().getSeconds(); //To get the Current Seconds
    var  Alltime=date+'/'+month+'/'+year+' '+hours+':'+min+':'+sec;
        firestore().collection(R).add({
            StartNeck: 'Start angle :'+_picMS,
            StopNeck:'Stop angle : '+_picME,
            MoveNeck:'Move angle : '+MOVE,
            timestamp: firestore.FieldValue.serverTimestamp(),
            timeNeck:Alltime
        })
}

  componentDidMount() {
    this._toggle();    
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  _toggle = () => {
    if (this._subscription) {
      this._unsubscribe();
    } else {
      this._subscribe();
    }
  };

  _subscribe = async () => {
    setUpdateIntervalForType(SensorTypes.magnetometer, 16);
    this._subscription = magnetometer.subscribe(
      sensorData => this.setState({magnetometer: this._angle(sensorData)}),
      error => console.log("The sensor is not available"),
    );
  };

  _unsubscribe = () => {
    this._subscription && this._subscription.unsubscribe();
    this._subscription = null;
  };

  _angle = magnetometer => {
    let angle = 0;
    if (magnetometer) {
      let {x, y} = magnetometer;
      // if (Math.atan2(y, x) >= 0) {
      //   angle = Math.atan2(y, x) * (180 / Math.PI);
      // } else {
      //   angle = Math.atan2(y, x) * (180 / Math.PI);
      // }
      angle = Math.atan2(y, x) * (180/Math.PI);
    }
    return Math.round(LPF.next(angle));
  };

  _direction = degree => {

  };

  // Match the device top with pointer 0° degree. (By default 0° starts from the right of the device.)
  _degree = magnetometer => {
   

    if(magnetometer<0){
      magnetometer= magnetometer+360;
    }
    else{
      magnetometer;
    }

    return  magnetometer-MET;
  };

  render() {
    return (
     <Grid style={{backgroundColor: "white"}}>  
        <Row style={{alignItems: "center"}} size={0.5}>
          <Col style={{alignItems: "center"}}>
            <Text
              style={{
                color: "#000000",
                fontSize: height / 26,
                fontWeight: "bold",
              }}
            >
              {this._degree(this.state.magnetometer)}°
            </Text>
          </Col>
        </Row>

        <Row style={{alignItems: "center"}} size={0.02}>
          <Col style={{alignItems: "center"}}>
            <View style={{width: width, alignItems: "center", bottom: 0}}>
              <Image
                source={require("../assets/compass_pointer.png")}
                style={{
                  height: height / 25,
                  resizeMode: "contain",
                }}
              />
            </View>
          </Col>
        </Row>

        <Row style={{alignItems: "center"}} size={1.4}>
          <Text
            style={{
              color: "#000000",
              fontSize: height / 27,
              width: width,
              position: "absolute",
              textAlign: "center",
            }}
          >
            {valueDG=this._degree(this.state.magnetometer)}°
          </Text>

          <Col style={{alignItems: "center"}}>
            <Image
              source={require("../assets/angleA.png")}
              style={{
                height: width - 100,
                justifyContent: "center",
                alignItems: "center",
                resizeMode: "contain",
                transform: [
                  {rotate: 360 - this.state.magnetometer + "deg"},
                  // {rotate: 360 - this.state.magnetometer + "deg"},
                ],
              }}
            />
          </Col>
        </Row>      

        <Text style={{
          color: '#00000',
           fontSize: height / 60,
          // width: width,
          // position: 'absolute',
           textAlign: 'center'
        }}>
          Start  : {_picMS} °
          </Text>



          <Text style={{
          color: '#00000',
           fontSize: height / 60,
          // width: width,
          // position: 'absolute',
           textAlign: 'center'
        }}>
          Stop  : {_picME} °
          </Text>
        <Text style={{
          color: '#000000',
           fontSize: height / 50,
          // width: width,
          // position: 'absolute',
           textAlign: 'center'
        }}>
          Move  : {MOVE=_picMS-_picME} °
          </Text>

          <View style={{ flexDirection: 'row',marginLeft:'auto', marginRight:'auto',marginTop:30}}>
<View>
<Button title=' Left '  onPress={() => {MET=360}}/>  
  </View>
  <View style={{marginLeft:50}}>
  <Button title='Right'  onPress={() => {MET=0} }/>  
  </View>
</View>


      

          <Row style={{alignItems: "center"}} size={1}>
          <Col style={{alignItems: "center"}} >
          <View style={[{ width: "70%", margintop: 10 }]}>
          <Button
           onPress={() => stbt() }
           color={colorBT}
           title= {stst}
           
          />
        </View>  

        <View style={[{ width: "70%", margin: 10 }]}>
         
         
         
          <Button
           color="#737373"
           title= "Save"  
           onPress={() => this.storeUser() }
          />






        </View>  

          </Col>

      
        </Row>


      </Grid>
    
    );
  }
}

const stbt = () => {
  
  if(st_button==true){
    
    stst="START";
    st_button=false;
    colorBT="#000080"
    CA=1;
    _picMS =valueDG;
  }
  else{
    stst="STOP";
    st_button=true;
    colorBT="#ff0000"
    _picME =valueDG;
  }
 
}

const styles =StyleSheet.create({
  container:{
      flex:1,
      padding:20
  },
  preloader:{
      position:'absolute',
      top:0,
      left:0,
      right:0,
      bottom:0,
      alignItems:'center',
      justifyContent:'center'
  }
})

const theme = {
  Button:{
      raised: true
  }
}


// import React, {Component} from "react";
// import {Text, View, Image, Dimensions,StyleSheet,Button,Alert,SafeAreaView,ScrollView,ActivityIndicator} from "react-native";
// import { ThemeProvider } from 'react-native-elements'
// import {Grid, Col, Row} from "react-native-easy-grid";
// import {magnetometer, SensorTypes, setUpdateIntervalForType} from "react-native-sensors";
// import LPF from "lpf";
// import  Icon  from 'react-native-vector-icons/FontAwesome'

// const {height, width} = Dimensions.get("window");
// var _picMS=0;
// var _picME=0;
// var st_button=false;
// var stst="START";
// var colorBT="#000080";
// var valueDG;
// var R;


// export default class MagnetometerPage extends Component {
//   constructor() {
//     super();
//     this.state = {
//       magnetometer: "0",
//     };
//     LPF.init([]);
//     LPF.smoothing = 0.6;
//   }

//   componentDidMount() {
//     this._toggle();
//   }

//   componentWillUnmount() {
//     this._unsubscribe();
//   }

//   _toggle = () => {
//     if (this._subscription) {
//       this._unsubscribe();
//     } else {
//       this._subscribe();
//     }
//   };

//   _subscribe = async () => {
//     setUpdateIntervalForType(SensorTypes.magnetometer, 16);
//     this._subscription = magnetometer.subscribe(
//       sensorData => this.setState({magnetometer: this._angle(sensorData)}),
//       error => console.log("The sensor is not available"),
//     );
//   };

//   _unsubscribe = () => {
//     this._subscription && this._subscription.unsubscribe();
//     this._subscription = null;
//   };

//   _angle = magnetometer => {
//     let angle = 0;
//     if (magnetometer) {
//       let {x, y} = magnetometer;
//       // if (Math.atan2(y, x) >= 0) {
//       //   angle = Math.atan2(y, x) * (180 / Math.PI);
//       // } else {
//       //   angle = Math.atan2(y, x) * (180 / Math.PI);
//       // }
//       angle = Math.atan2(y, x) * (180/Math.PI);
//     }
//     return Math.round(LPF.next(angle));
//   };

//   _direction = degree => {

//   };

//   // Match the device top with pointer 0° degree. (By default 0° starts from the right of the device.)
//   _degree = magnetometer => {
   
//     if(magnetometer>180){
//         magnetometer=360-magnetometer;
//     }
    
//     return  magnetometer
//   };

//   render() {
//     return (


 
      
//       <Grid style={{backgroundColor: "white"}}>
  
//         <Row style={{alignItems: "center"}} size={0.5}>
//           <Col style={{alignItems: "center"}}>
//             <Text
//               style={{
//                 color: "#000000",
//                 fontSize: height / 26,
//                 fontWeight: "bold",
//               }}
//             >
//               {this._degree(this.state.magnetometer)}°
//             </Text>
//           </Col>
//         </Row>

//         <Row style={{alignItems: "center"}} size={0.02}>
//           <Col style={{alignItems: "center"}}>
//             <View style={{width: width, alignItems: "center", bottom: 0}}>
//               <Image
//                 source={require("../assets/compass_pointer.png")}
//                 style={{
//                   height: height / 25,
//                   resizeMode: "contain",
//                 }}
//               />
//             </View>
//           </Col>
//         </Row>

//         <Row style={{alignItems: "center"}} size={1.4}>
//           <Text
//             style={{
//               color: "#000000",
//               fontSize: height / 27,
//               width: width,
//               position: "absolute",
//               textAlign: "center",
//             }}
//           >
//             {valueDG=this._degree(this.state.magnetometer)}°
//           </Text>

//           <Col style={{alignItems: "center"}}>
//             <Image
//               source={require("../assets/angleA.png")}
//               style={{
//                 height: width - 100,
//                 justifyContent: "center",
//                 alignItems: "center",
//                 resizeMode: "contain",
//                 transform: [
//                   {rotate: 360 - this.state.magnetometer + "deg"},
//                 ],
//               }}
//             />
//           </Col>
//         </Row>
        
      


//         <Text style={{
//           color: '#00000',
//            fontSize: height / 60,
//           // width: width,
//           // position: 'absolute',
//            textAlign: 'center'
//         }}>
//           Start  : {_picMS} °
//           </Text>



//           <Text style={{
//           color: '#00000',
//            fontSize: height / 60,
//           // width: width,
//           // position: 'absolute',
//            textAlign: 'center'
//         }}>
//           Stop  : {_picME} °
//           </Text>
//         <Text style={{
//           color: '#000000',
//            fontSize: height / 50,
//           // width: width,
//           // position: 'absolute',
//            textAlign: 'center'
//         }}>
//           Move  : {_picMS-_picME} °
//           </Text>

       

      

//           <Row style={{alignItems: "center"}} size={1}>
//           <Col style={{alignItems: "center"}} >
//           <View style={[{ width: "70%", margintop: 10 }]}>
//           <Button
//            onPress={() => stbt() }
//            color={colorBT}
//            title= {stst}
           
//           />
//         </View>  

//         <View style={[{ width: "70%", margin: 10 }]}>
         
         
         
//           <Button
//            color="#737373"
//            title= "Save"
//           onPress
//           />






//         </View>  

//           </Col>

      
//         </Row>


//       </Grid>
    
//     );
//   }
// }

// const stbt = () => {
  
//   if(st_button==true){
//     stst="START";
//     st_button=false;
//     colorBT="#000080"
    
//     _picMS =valueDG;
//   }
//   else{
//     stst="STOP";
//     st_button=true;
//     colorBT="#ff0000"
//     _picME =valueDG;
//   }
 
// }

// const styles =StyleSheet.create({
//   container:{
//       flex:1,
//       padding:20
//   },
//   preloader:{
//       position:'absolute',
//       top:0,
//       left:0,
//       right:0,
//       bottom:0,
//       alignItems:'center',
//       justifyContent:'center'
//   }
// })

// const theme = {
//   Button:{
//       raised: true
//   }
// }