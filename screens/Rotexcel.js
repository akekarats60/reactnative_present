

import XLSX from 'xlsx';

import React, { Component,useState } from 'react';
import { PermissionsAndroid,StyleSheet, Text, View, Button, Alert, Image, ScrollView, TouchableWithoutFeedback,Platform} from 'react-native';
import { Table, Row, Rows, TableWrapper } from 'react-native-table-component';

import { Appbar, RadioButton,Divider } from 'react-native-paper';
import firestore from '@react-native-firebase/firestore';
import { ListItem,Badge} from 'react-native-elements'

// react-native-fs
import {readFile,writeFile, DocumentDirectoryPath,DownloadDirectoryPath } from 'react-native-fs';


const DDP = DownloadDirectoryPath + "/";

var SL2=[];
var LL2=[];
var LR2=[];
var SL02=[];
var LL02=[];
var LR02=[];

var SL12=[];
var LL12=[];
var LR12=[];
var SL012=[];
var LL012=[];
var LR012=[];

var SL22=[];
var LL22=[];
var LR22=[];
var SL022=[];
var LL022=[];
var LR022=[];

var AASL2=[];
var AALL2=[];
var AALR2=[];
var AASL02=[];
var AALL02=[];
var AALR02=[];

var LAS;
var II;


var picStrat=1;
var ii=[];

const input = res => res;
const output = str => str;
var nameUser;

export default class Rotexcel extends Component {
	constructor({props,route}) {
        SL2=[];
        LL2=[];
        LR2=[];
        
        SL12=[];
        LL12=[];
        LR12=[];
        
        SL22=[];
        LL22=[];
        LR22=[];
        
        AASL2=[];
        AALL2=[];
        AALR2=[];

		super(props);
		this.dbRef = firestore();
		this.state = {isLoading: false,
            userArr:[],
			userArr2:[],
			userArr3:[],

			data: [["1","2","3"],
      [1,2,3],
      [1,2,3],
      [1,2,3],
      [1,2,3],
      [1,2,3],],

		};
		
		this.exportFile = this.exportFile.bind(this);

		const {itemId} = route.params;
        console.log(itemId);
        nameUser=itemId;
	};

	


    componentDidMount(){  
        console.log('this one call');
        this.unsubscribe = this.dbRef.collection(nameUser).orderBy("timestamp").onSnapshot(this.getCollection);
      }


	  getCollection=(querySnapshot)=>{
        const userArr=[];
        querySnapshot.forEach((res) => {
            const {    
                RSF1,
                RSTF1,
                RM1,
        
                RSF2,
                RSTF2,
                RM2,
        
                RSF3,
                RSTF3,
                RM3,
                
                RSFA,
                RSTFA,
                RMA,
                
                RtimeNeckFlex} =res.data();
            userArr.push({
                key: res.id,
                res,
               
                RSF1,
                RSTF1,
                RM1,
        
                RSF2,
                RSTF2,
                RM2,
        
                RSF3,
                RSTF3,
                RM3,
                
                RSFA,
                RSTFA,
                RMA,
                
                RtimeNeckFlex
                
            })
            this.setState({
                userArr,
                isLoading:false
            })
        });
        calcuRot();
	}


requestRunTimePermission=()=>{
  var that =this;
  async function externalStoragePermission(){
try{
  const granted = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
    {
      title :"External Storage Write Permission",
      message :"App needs access to Storage data.",
    }
  );
  if(granted === PermissionsAndroid.RESULTS.GRANTED){
    that.exportFile();
	start=[];
  }else{
    alert('WRITE_EXTERNAL_STORAGE permission denied');
  }
}catch (err){
  Alert,alert('Write permission err',err);
}
}

if(Platform.OS === 'android'){
  externalStoragePermission();
}else{
  this.exportFile();
}
}
exportFile() {
		var date = new Date().getDate(); //To get the Current Date
		var month = new Date().getMonth(); //To get the Current Month
		var year = new Date().getFullYear(); //To get the Current Year
		var hours = new Date().getHours(); //To get the Current Hours
		var min = new Date().getMinutes(); //To get the Current Minutes
		var sec = new Date().getSeconds(); //To get the Current Seconds
		var  Alltime=date+'|'+month+'|'+year;
	
		var data=[];
        for (let index = 0; index < SL2.length; index++) {
          if(typeof(SL2[index]) !== 'undefined' && SL2[index] != null ||typeof(SL12[index]) !== 'undefined' && SL12[index] != null||typeof(SL22[index]) !== 'undefined' && SL22[index] != null||typeof(AASL2[index]) !== 'undefined' && AASL2[index] != null){            
            data.push(
            {"No":"1","Start":SL2[index],"Left":LL2[index],"Right":LR2[index]},
            {"No":"2","Start":SL12[index],"Left":LL22[index],"Right":LR22[index]},
            {"No":"3","Start":SL22[index],"Left":LL22[index],"Right":LR22[index]},
            {"No":"avg","Start":AASL2[index],"Left":AALL2[index],"Right":AALR2[index]},
            {"":""},
            );
}
        }
		const ws = XLSX.utils.json_to_sheet(data);

		/* build new workbook */
		const wb = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(wb, ws, "SheetJS");

		/* write file */
		
		const G="RotSheet"+Alltime+".xlsx";
		
		const wbout = XLSX.write(wb, {type:'binary', bookType:"xlsx"});
		const file = DDP + G ;

		writeFile(file, output(wbout), 'ascii').then((res) =>{
				Alert.alert("exportFile success", "Exported to " + file);
		}).catch((err) => { Alert.alert("exportFile Error", "Error " + err.message); });

	};
	




	render() { return (


<ScrollView>

				 {
				
                      this.state.userArr.map((item,i)=>{
                     
						
					    ii.push(picStrat);
						II=ii.length;
						LAS=ii.length-(i+1);

                        SL02.push(item.RSF1);
                        LL02.push(item.RSTF1);
                        LR02.push(item.RM1);
      
                        SL012.push(item.RSF2);
                        LL012.push(item.RSTF2);
                        LR012.push(item.RM2);
      
                        SL022.push(item.RSF3);
                        LL022.push(item.RSTF3);
                        LR022.push(item.RM3);
      
                        AASL02.push(item.RSFA);
                        AALL02.push(item.RSTFA);
                        AALR02.push(item.RMA);

					
                       })
					   
                  }

<Text style={styles.welcome}>EXPORT DATA TO EXCEL FILE</Text>

<View style={{marginTop:10}}>
<Button onPress={()=>{this.requestRunTimePermission()}} 
            title="Export data" color="#008080" />
</View>
<Text>Rotation</Text>


</ScrollView>
	); };
};

const styles = StyleSheet.create({
	container: { flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#F5FCFF' },
	welcome: { fontSize: 20, textAlign: 'center', margin: 10 },
	instructions: { textAlign: 'center', color: '#333333', marginBottom: 5 },
	thead: { height: 40, backgroundColor: '#f1f8ff' },
	tr: { height: 30 },
	text: { marginLeft: 5 },
	table: { width: "100%" }
});

function calcuRot(){
	for (let index = LAS; index < II; index++) {
		SL2.push(SL02[index]);	
		LL2.push(LL02[index]);
		LR2.push(LR02[index]);
	}
	for (let index = LAS; index < II; index++) {
		SL12.push(SL012[index]);	
		LL12.push(LL012[index]);
		LR12.push(LR012[index]);
	}
	for (let index = LAS; index < II; index++) {
		SL22.push(SL022[index]);	
		LL22.push(LL022[index]);
		LR22.push(LR022[index]);
	}
	for (let index = LAS; index < II; index++) {
		AASL2.push(AASL02[index]);	
		AALL2.push(AALL02[index]);
		AALR2.push(AALR02[index]);
	}
}







// import XLSX from 'xlsx';

// import React, { Component,useState } from 'react';
// import { PermissionsAndroid,StyleSheet, Text, View, Button, Alert, Image, ScrollView, TouchableWithoutFeedback,Platform} from 'react-native';
// import { Table, Row, Rows, TableWrapper } from 'react-native-table-component';

// import { Appbar, RadioButton,Divider } from 'react-native-paper';
// import firestore from '@react-native-firebase/firestore';
// import { ListItem,Badge} from 'react-native-elements'

// // react-native-fs
// import {readFile,writeFile, DocumentDirectoryPath,DownloadDirectoryPath } from 'react-native-fs';


// const DDP = DownloadDirectoryPath + "/";

// var SL2=[];
// var LL2=[];
// var LR2=[];
// var SL02=[];
// var LL02=[];
// var LR02=[];

// var SL12=[];
// var LL12=[];
// var LR12=[];
// var SL012=[];
// var LL012=[];
// var LR012=[];

// var SL22=[];
// var LL22=[];
// var LR22=[];
// var SL022=[];
// var LL022=[];
// var LR022=[];

// var AASL2=[];
// var AALL2=[];
// var AALR2=[];
// var AASL02=[];
// var AALL02=[];
// var AALR02=[];


// var LAS3;
// var II3;


// var picStrat3=1;
// var ii3=[];

// const input = res => res;
// const output = str => str;
// var nameUser;

// export default class RotExcel extends Component {
// 	constructor({props,route}) {
//         SL2=[];
//         LL2=[];
//         LR2=[];
        
//         SL12=[];
//         LL12=[];
//         LR12=[];
        
//         SL22=[];
//         LL22=[];
//         LR22=[];
        
//         AASL2=[];
//         AALL2=[];
//         AALR2=[];


// 		super(props);
// 		this.dbRef = firestore();
// 		this.state = {isLoading: false,
//             userArr:[],
	

// 			data: [["1","2","3"],
//       [1,2,3],
//       [1,2,3],
//       [1,2,3],
//       [1,2,3],
//       [1,2,3],],

// 		};
		
// 		this.exportFile3 = this.exportFile3.bind(this);

// 		const {itemId} = route.params;
//         console.log(itemId);
//         nameUser=itemId;
// 	};

	


//     componentDidMount(){  
//         console.log('this one call');
//         this.unsubscribe = this.dbRef.collection(nameUser).orderBy("timestamp").onSnapshot(this.getCollection3);
//       }

//       getCollection3=(querySnapshot)=>{
//         const userArr=[];
//         querySnapshot.forEach((res) => {
//             const {    
//                 RSF1,
//                 RSTF1,
//                 RM1,
        
//                 RSF2,
//                 RSTF2,
//                 RM2,
        
//                 RSF3,
//                 RSTF3,
//                 RM3,
                
//                 RSFA,
//                 RSTFA,
//                 RMA,
                
//                 RtimeNeckFlex} =res.data();
//             userArr.push({
//                 key: res.id,
//                 res,
               
//                 RSF1,
//                 RSTF1,
//                 RM1,
        
//                 RSF2,
//                 RSTF2,
//                 RM2,
        
//                 RSF3,
//                 RSTF3,
//                 RM3,
                
//                 RSFA,
//                 RSTFA,
//                 RMA,
                
//                 RtimeNeckFlex
                
//             })
//             this.setState({
//                 userArr,
//                 isLoading:false
//             })
//         });
//         calcuRot();
//         }

// requestRunTimePermission3=()=>{
//   var that =this;
//   async function externalStoragePermission3(){
// try{
//   const granted = await PermissionsAndroid.request(
//     PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
//     {
//       title :"External Storage Write Permission",
//       message :"App needs access to Storage data.",
//     }
//   );
//   if(granted === PermissionsAndroid.RESULTS.GRANTED){
//     that.exportFile3();

//   }else{
//     alert('WRITE_EXTERNAL_STORAGE permission denied');
//   }
// }catch (err){
//   Alert,alert('Write permission err',err);
// }
// }

// if(Platform.OS === 'android'){
//   externalStoragePermission3();
// }else{
//   this.exportFile3();
// }
// }
// exportFile3() {
// 		var date = new Date().getDate(); //To get the Current Date
// 		var month = new Date().getMonth(); //To get the Current Month
// 		var year = new Date().getFullYear(); //To get the Current Year
// 		var hours = new Date().getHours(); //To get the Current Hours
// 		var min = new Date().getMinutes(); //To get the Current Minutes
// 		var sec = new Date().getSeconds(); //To get the Current Seconds
// 		var  Alltime=date+'|'+month+'|'+year;
	
// 		var data=[];
//         for (let index = 0; index < SL2.length; index++) {
//             data.push(
//             {"No":"1","Start":SL2[index],"Left":LL2[index],"Right":LR2[index]},
//             {"No":"2","Start":SL12[index],"Left":LL22[index],"Right":LR22[index]},
//             {"No":"3","Start":SL22[index],"Left":LL22[index],"Right":LR32[index]},
//             {"No":"avg","Start":AASL2[index],"Left":AALL2[index],"Right":AALR2[index]},
//             {"":""},
//             );
// }
// 		const ws = XLSX.utils.json_to_sheet(data);

// 		/* build new workbook */
// 		const wb = XLSX.utils.book_new();
// 		XLSX.utils.book_append_sheet(wb, ws, "SheetJS");

// 		/* write file */
		
// 		const G="Sheet"+Alltime+".xlsx";
		
// 		const wbout = XLSX.write(wb, {type:'binary', bookType:"xlsx"});
// 		const file = DDP + G ;

// 		writeFile(file, output(wbout), 'ascii').then((res) =>{
// 				Alert.alert("exportFile success", "Exported to " + file);
// 		}).catch((err) => { Alert.alert("exportFile Error", "Error " + err.message); });

// 	};
	




// 	render() { return (


// <ScrollView>

//             {
				

// 				this.state.userArr.map((item,i)=>{
	
// 				  ii3.push(picStrat3);
// 				  II3=ii3.length;
// 				  LAS3=ii3.length-(i+1);

// 				  SL02.push(item.RSF1);
// 				  LL02.push(item.RSTF1);
// 				  LR02.push(item.RM1);

// 				  SL012.push(item.RSF2);
// 				  LL012.push(item.RSTF2);
// 				  LR012.push(item.RM2);

// 				  SL022.push(item.RSF3);
// 				  LL022.push(item.RSTF3);
// 				  LR022.push(item.RM3);

// 				  AASL02.push(item.RSFA);
// 				  AALL02.push(item.RSTFA);
// 				  AALR02.push(item.RMA);
// 				 })
				 
// 			}


// <Text style={styles.welcome}>EXPORT DATA TO EXCEL FILE</Text>

// <View style={{marginTop:10}}>
// <Button onPress={()=>{this.requestRunTimePermission3()}} 
//             title="Export data" color="#008000" />
// </View>
// <Text>Rotation</Text>

// </ScrollView>
// 	); };
// };

// const styles = StyleSheet.create({
// 	container: { flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#F5FCFF' },
// 	welcome: { fontSize: 20, textAlign: 'center', margin: 10 },
// 	instructions: { textAlign: 'center', color: '#333333', marginBottom: 5 },
// 	thead: { height: 40, backgroundColor: '#f1f8ff' },
// 	tr: { height: 30 },
// 	text: { marginLeft: 5 },
// 	table: { width: "100%" }
// });

// function calcuRot(){
// 	for (let index = LAS3; index < II3; index++) {
// 		SL2.push(SL02[index]);	
// 		LL2.push(LL02[index]);
// 		LR2.push(LR02[index]);
// 	}
// 	for (let index = LAS3; index < II3; index++) {
// 		SL12.push(SL012[index]);	
// 		LL12.push(LL012[index]);
// 		LR12.push(LR012[index]);
// 	}
// 	for (let index = LAS3; index < II3; index++) {
// 		SL22.push(SL022[index]);	
// 		LL22.push(LL022[index]);
// 		LR22.push(LR022[index]);
// 	}
// 	for (let index = LAS3; index < II3; index++) {
// 		AASL2.push(AASL02[index]);	
// 		AALL2.push(AALL02[index]);
// 		AALR2.push(AALR02[index]);
// 	}
// }

