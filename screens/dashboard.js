import React from "react";
import {TouchableOpacity,Dimensions,StyleSheet,ScrollView,ImageBackground,ActivityIndicator,View,Text,ImageBackgroundBase} from 'react-native'
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Container,ThemeProvider,Button, Input, Image } from 'react-native-elements'
import  Icon  from 'react-native-vector-icons/FontAwesome'
import { BackgroundImage } from 'react-native-elements/dist/config'
import { NativeModules } from "react-native";
import { Appbar } from 'react-native-paper';
import AllNeck from "./AllNeck";




const rows = 3;
const cols = 2;
const marginHorizontal = 4;
const marginVertical = 4;

const width = (Dimensions.get('window').width / cols) - (marginHorizontal * (cols + 1));
const height = (Dimensions.get('window').height / rows) - (marginVertical * (rows + 1));



function Dashboard({route,navigation}){
  const {itemId} = route.params;

    return(
//         <ThemeProvider theme={theme}>
//                 <ScrollView style={styles.container}>

// <View style={styles2.container}>
//       <TouchableOpacity onPress={() => navigation.navigate("AccelerometerPage")}>
//         <ImageBackground source={require("./assets/images.png")} style={{ padding: 30,height:150,width:150}}>
//           <Text style={styles2.title}>Arm</Text>
//         </ImageBackground>
//       </TouchableOpacity>

//       <TouchableOpacity onPress={() => navigation.navigate("AccelerometerPage")}>
//         <ImageBackground source={require("./assets/neck.jpg")} style={{ padding: 30,height:150,width:150}}>
//           <Text style={styles2.title}>Neck</Text>
//         </ImageBackground>
//       </TouchableOpacity>

//     </View>

    

            
//                 </ScrollView>   
//             </ThemeProvider>


<ScrollView>
<Appbar.Header  style={{backgroundColor:'#f0f8ff',marginTop:2}}>
      
      
      <Appbar.Content title='ยินดีต้อนรับ' subtitle={itemId} />
     
  <View style={{ marginRight:15}}>
  
  <Button   onPress={() => NativeModules.DevSettings.reload()}
      icon={
        <Icon 
        name='sign-out'
        size={25}
        color='#000000'
        />
    }
  
    buttonStyle={{
      backgroundColor: "#f0f8ff"
  }}
  />
</View>

</Appbar.Header>



{/* <View  style={{ flexDirection: 'row', flex:1,marginLeft:25, marginRight:'auto',marginTop:10}}>
  <View>
  <Text style={{fontSize:16,fontWeight: "bold"}}>
      สวัสดีคุณ : {itemId}     
  </Text>
  <Text style={{fontSize:12}}>
      ยินดีต้อนรับสู่ระบบการวัดพิสัยการเคลื่อนที่
  </Text>
  </View>
</View> */}

  {/* <View  style={{ flexDirection: 'row', flex:1,marginLeft:20, marginRight:'auto'}}>
  <Text style={{fontSize:12}}>
      ยินดีต้อนรับสู่ระบบการวัดพิสัยการเคลื่อนที่
  </Text>
  </View> */}





<View style={{ flexDirection: 'row', flex:1,marginLeft:'auto', marginRight:'auto',marginTop:20}}>

<TouchableOpacity onPress={() => navigation.navigate("AccelerometerPage", {itemId:itemId,})}>
        <ImageBackground source={require("../assets/images.png")} style={{padding:0,height:200,width:160,}}>
          <Text style={styles2.title}> ARM</Text>
          <Text style={{fontSize:11}}> Accelerometer</Text>
          <View style={{marginTop:121}}>
          <Button  title=' Result' onPress={() => navigation.navigate("ArmPage", {itemId:itemId,})}
      icon={
        <Icon 
        name='list-alt'
        size={15}
        color='white'
        />
    }
  
    buttonStyle={{
      backgroundColor: "#5f9ea0"
  }}
  />
  </View>
        </ImageBackground>
        
      </TouchableOpacity>


<TouchableOpacity onPress={() => navigation.navigate("AllNeck", {itemId:itemId,})}>
        <ImageBackground source={require("../assets/neck.jpg")} style={{padding:0,height:200,width:160,marginLeft:20}}>
          <Text style={styles2.title}> NECK</Text>
          <Text style={{fontSize:11}}> Magnetometer</Text>
          <View style={{marginTop:121}}>
          <Button   title=' Result' onPress={() => navigation.navigate("excelExport", {itemId:itemId,})}
      icon={
        <Icon 
        name='list-alt'
        size={15}
        color='white'
        />
    }
  
    buttonStyle={{
      backgroundColor: "#5f9ea0"
  }}
  />
  </View>
        </ImageBackground>
        <View style={{marginleft:10}}>
      
        </View>
  
      </TouchableOpacity>


</View>
</ScrollView>
)
  }
  
  const theme = {
    Button:{
        raised: true
    }
}

const styles =StyleSheet.create({
    container:{
        flex:1,
        padding:20
    },
    preloader:{
        position:'absolute',
        top:0,
        left:0,
        right:0,
        bottom:0,
        alignItems:'center',
        justifyContent:'center'
    },
})



const styles2 = StyleSheet.create({
  container: {
    

   
  },
  title: {
    
    color: "#808080",
    fontSize: 18,
    fontWeight: "bold",
  }
});

const stylesGrid = StyleSheet.create({
  scrollContainer: {
    flex: 1,
  },
  sectionContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
  },
  boxContainer: {
    marginTop: marginVertical,
    marginBottom: marginVertical,
    marginLeft: marginHorizontal,
    marginRight: marginHorizontal,
    width: width,
    height: height,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'gold',
  },
});
export default Dashboard;





