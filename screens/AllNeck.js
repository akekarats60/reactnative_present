import React from "react";
import {TouchableOpacity,Dimensions,StyleSheet,ScrollView,ImageBackground,ActivityIndicator,View,Text,ImageBackgroundBase} from 'react-native'
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Container,ThemeProvider,Button, Input, Image } from 'react-native-elements'
import  Icon  from 'react-native-vector-icons/FontAwesome'
import { BackgroundImage } from 'react-native-elements/dist/config'
import { NativeModules } from "react-native";
import { Appbar, RadioButton,Divider } from 'react-native-paper';

var date = new Date().getDate(); //To get the Current Date
var month = new Date().getMonth() + 1; //To get the Current Month
var year = new Date().getFullYear(); //To get the Current Year
var hours = new Date().getHours(); //To get the Current Hours
var min = new Date().getMinutes(); //To get the Current Minutes
var sec = new Date().getSeconds(); //To get the Current Seconds


const rows = 3;
const cols = 2;
const marginHorizontal = 4;
const marginVertical = 4;

const width = (Dimensions.get('window').width / cols) - (marginHorizontal * (cols + 1));
const height = (Dimensions.get('window').height / rows) - (marginVertical * (rows + 1));
var NUMBER;





function AllNeck({route,navigation}){
  const {itemId} = route.params;
  const [checked, setChecked] = React.useState('first');
    return(
<ScrollView>
<Appbar.Header  style={{backgroundColor:'#d3d3d3',marginTop:5}}>
      
      
      <Appbar.Content title='NECK' subtitle={'กิจกรรมบำบัดส่วนคอของคุณ '+itemId} />

      <Appbar.Action icon=""/>
      
  <View style={{ marginRight:15}}>
  <Button   onPress={() => NativeModules.DevSettings.reload()}
      icon={
        <Icon 
        name='sign-out'
        size={25}
        color='#000000'
        />
    }
  
    buttonStyle={{
      backgroundColor: "#d3d3d3"
  }}
  />
</View>




</Appbar.Header>



<View style={{ flexDirection: 'row', flex:1,marginLeft:10,marginTop:10,marginBottom:10}}>
      
      <Text style={{marginTop:'auto',marginBottom:'auto',marginRight:10,fontSize:16,fontWeight: "bold",}}>
          NUMBER : 
      </Text>


      <Text style={{marginTop:'auto',marginBottom:'auto',marginLeft:3}}>
          1  
      </Text>
      <RadioButton
        value="first"
        status={ checked === 'first' ? 'checked' : 'unchecked' }
        onPress={() => setChecked('first')}
      />
    <Text style={{marginTop:'auto',marginBottom:'auto',marginLeft:10}}>
          2 
      </Text>
      <RadioButton
        value="second"
        status={ checked === 'second' ? 'checked' : 'unchecked' }
        onPress={() => setChecked('second')}
      />
   <Text style={{marginTop:'auto',marginBottom:'auto',marginLeft:10}}>
          3  
      </Text>
      <RadioButton
        value="first"
        status={ checked === 'third' ? 'checked' : 'unchecked' }
        onPress={() => setChecked('third')}
      />


    </View>
{checkBT(checked)}
    <Divider />




<View style={{ flexDirection: 'row', flex:1,marginLeft:10,marginTop:10,marginBottom:10}}>

        <Text style={{fontSize:16,fontWeight:'bold'}}>
            MOVEMENT 
        </Text>
</View>






  
<View style={{ flexDirection: 'column', flex:1,marginLeft:'auto', marginRight:'auto',marginTop:10}}>

<TouchableOpacity onPress={() => navigation.navigate("FlexPage", {itemId:itemId,NUM:NUMBER})}>
        <ImageBackground source={require("../assets/Flex.png")} style={{padding:0,height:140,width:350,marginLeft:0}}>
          <Text style={styles2.title}>Flextion/Extention</Text>
          <Text style={{fontSize:11}}> Acceleromete</Text>
         
 <View style={{marginTop:60,flexDirection: 'row', flex:1}}>
  <View style={{width:300}}>
   <Button   title=' Result' onPress={() => navigation.navigate("Flexdata", {itemId:itemId,NUM:NUMBER})}
      icon={
        <Icon 
        name='list-alt'
        size={15}
        color='white'
        />
     } 
    buttonStyle={{
      backgroundColor: "#5f9ea0"
    }}
    />
</View>          
<View style={{ marginLeft:'auto',marginRight:0}}>
  <Button  onPress={() =>  navigation.navigate("excelExport", {itemId:itemId})}
      icon={
        <Icon 
        name='save'
        size={15}
        color='#f8f8ff'
        />
    }
    title=" Excel" 
    buttonStyle={{
      backgroundColor: "#008080"
  }}
  />
</View>
</View>
        </ImageBackground>
        <View style={{marginleft:10}}>
      
        </View>
  
      </TouchableOpacity>




      <TouchableOpacity onPress={() => navigation.navigate("LetPage", {itemId:itemId,NUM:NUMBER})}>
        <ImageBackground source={require("../assets/Lt.png")} style={{padding:0,height:140,width:350,marginLeft:0, marginTop:15}}>
          <Text style={styles2.title}>Letterral</Text>
          <Text style={{fontSize:11}}> Acceleromete</Text>
          <View style={{marginTop:60,flexDirection: 'row', flex:1}}>
  <View style={{width:300}}>
   <Button   title=' Result' onPress={() => navigation.navigate("Letdata", {itemId:itemId,NUM:NUMBER})}
      icon={
        <Icon 
        name='list-alt'
        size={15}
        color='white'
        />
     } 
    buttonStyle={{
      backgroundColor: "#5f9ea0"
    }}
    />
</View>          
<View style={{ marginLeft:'auto',marginRight:0}}>
  <Button  onPress={() =>  navigation.navigate("LetExcel", {itemId:itemId})}
      icon={
        <Icon 
        name='save'
        size={15}
        color='#f8f8ff'
        />
    }
    title=" Excel" 
    buttonStyle={{
      backgroundColor: "#008080"
  }}
  />
</View>
</View>

        </ImageBackground>
        <View style={{marginleft:10}}>
      
        </View>
  
      </TouchableOpacity>


      <TouchableOpacity onPress={() => navigation.navigate("RotPage", {itemId:itemId,NUM:NUMBER})}>
        <ImageBackground source={require("../assets/Ro.png")} style={{padding:0,height:140,width:350,marginLeft:0, marginTop:15,marginBottom:20}}>
          <Text style={styles2.title}>Rotation</Text>
          <Text style={{fontSize:11}}> Magnetometer</Text>
         
         
<View style={{marginTop:60,flexDirection: 'row', flex:1}}>
  <View style={{width:300}}>
   <Button   title=' Result' onPress={() => navigation.navigate("Rotdata", {itemId:itemId,NUM:NUMBER})}
      icon={
        <Icon 
        name='list-alt'
        size={15}
        color='white'
        />
     } 
    buttonStyle={{
      backgroundColor: "#5f9ea0"
    }}
    />
</View>          
<View style={{ marginLeft:'auto',marginRight:0}}>
  <Button  onPress={() =>   navigation.navigate("RotExcel", {itemId:itemId})}
      icon={
        <Icon 
        name='save'
        size={15}
        color='#f8f8ff'
        />
    }
    title=" Excel" 
    buttonStyle={{
      backgroundColor: "#008080"
  }}
  />
</View>
</View>



        </ImageBackground>
        <View style={{marginleft:10}}>
      
        </View>
  
      </TouchableOpacity>


      
</View>

<Divider />


</ScrollView>
)
  }
  
  const theme = {
    Button:{
        raised: true
    }
}

const styles =StyleSheet.create({
    container:{
        flex:1,
        padding:20
    },
    preloader:{
        position:'absolute',
        top:0,
        left:0,
        right:0,
        bottom:0,
        alignItems:'center',
        justifyContent:'center'
    },
})



const styles2 = StyleSheet.create({
  container: {
    

   
  },
  title: {
    
    color: "#808080",
    fontSize: 18,
    fontWeight: "bold",
  }
});

const stylesGrid = StyleSheet.create({
  scrollContainer: {
    flex: 1,
  },
  sectionContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
  },
  boxContainer: {
    marginTop: marginVertical,
    marginBottom: marginVertical,
    marginLeft: marginHorizontal,
    marginRight: marginHorizontal,
    width: width,
    height: height,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'gold',
  },
});


function checkBT(checked){
    if(checked=='first'){
        NUMBER=1;
    }
    else if(checked=='second'){
        NUMBER=2;
    }
    else{
        NUMBER=3;
    }
    console.log(NUMBER);
}
export default AllNeck;





