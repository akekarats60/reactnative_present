import React, {Component,useState, useEffect} from "react";
import {Text, View, Image, Dimensions,StyleSheet,ActivityIndicator,Button,Alert,SafeAreaView,ScrollView} from "react-native";
import {Grid, Col, Row} from "react-native-easy-grid";
import {magnetometer, SensorTypes, setUpdateIntervalForType} from "react-native-sensors";
import LPF from "lpf";
import  Icon  from 'react-native-vector-icons/FontAwesome'
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import { ThemeProvider} from 'react-native-elements'
import firestore from '@react-native-firebase/firestore';
import { DataTable } from 'react-native-paper';

const {height, width} = Dimensions.get("window");
var _picMS=0;
var _picME=0;
var st_button=0;
var st_AG='PREPARING';
var stst="START";
var colorBT="#5f9ea0";
var valueDG;
var RR;
var picAngle;
var ACCT=0;
var MOVE;
var st_button2=0;

var XXX=0;
var YYY=0;
var C=0;

var CC;
var CCC;
var CCCC;


var CC1;
var CCC1;
var CCCC1;

var CC2;
var CCC2;
var CCCC2;

var CC3;
var CCC3;
var CCCC3;

var AVG1,AVG2,AVG3;


var i=0;
var T=[];
var TT=[];
var TTT=[];

var sent=false;

var RoundO123;
var cloneRound;


export default class RotPage extends Component {
  
  constructor({route}) {
        super();
    this.state = {
      magnetometer: "0",
      
    };
    LPF.init([]);
    LPF.smoothing = 0.4;
    const {itemId,NUM} = route.params;
    RR=itemId;
    st_AG='PREPARING';
    RoundO123=NUM;
    cloneRound=NUM;
  // console.log(itemId);
  // console.log(NUM);
  }


  storeUser22() {
    var date = new Date().getDate(); //To get the Current Date
    var month = new Date().getMonth(); //To get the Current Month
    var year = new Date().getFullYear(); //To get the Current Year
    var hours = new Date().getHours(); //To get the Current Hours
    var min = new Date().getMinutes(); //To get the Current Minutes
    var sec = new Date().getSeconds(); //To get the Current Seconds
    var  Alltime=date+'/'+month+'/'+year+' '+hours+':'+min+':'+sec;
CC1=Math.abs(CC1);
CCC1=Math.abs(CCC1);
CCCC1=Math.abs(CCCC1);

CC2=Math.abs(CC2);
CCC2=Math.abs(CCC2);
CCCC2=Math.abs(CCCC2);

CC3=Math.abs(CC3);
CCC3=Math.abs(CCC3);
CCCC3=Math.abs(CCCC3);

AVG1=AVG1.toFixed(0);
AVG2=AVG2.toFixed(0);
AVG3=AVG3.toFixed(0);

     if(cloneRound==1){
      firestore().collection(RR).add({
        RSF1:CC1,
        RSTF1:CCC1,
        RM1:CCCC1,

        RSFA: AVG1,
        RSTFA:AVG2,
        RMA:AVG3,

        timestamp: firestore.FieldValue.serverTimestamp(),
        RtimeNeckFlex:Alltime
    }).then((res)=>{
                      this.setState({
                        RSF1:'',
                        RSTF1:'',
                        RM1:'',
                        RSFA: '',
                        RSTFA:'',
                        RMA:'',
                
                        timestamp:'',
                        RtimeNeckFlex:''
                      })
    })

  }
    else if(cloneRound==2){
      firestore().collection(RR).add({
        RSF1:CC1,
        RSTF1:CCC1,
        RM1:CCCC1,

        RSF2:CC2,
        RSTF2:CCC2,
        RM2:CCCC2,

        RSFA: AVG1,
        RSTFA:AVG2,
        RMA:AVG3,

        timestamp: firestore.FieldValue.serverTimestamp(),
        RtimeNeckFlex:Alltime
    })
    }
    else if(cloneRound==3){
      firestore().collection(RR).add({
        RSF1:CC1,
        RSTF1:CCC1,
        RM1:CCCC1,

        RSF2:CC2,
        RSTF2:CCC2,
        RM2:CCCC2,

        RSF3: CC3,
        RSTF3:CCC3,
        RM3:CCCC3,
        
        RSFA: AVG1,
        RSTFA:AVG2,
        RMA:AVG3,
        timestamp: firestore.FieldValue.serverTimestamp(),
        RtimeNeckFlex:Alltime

    })
    }   

}



  componentDidMount() {
    this._toggle();  
      
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  _toggle = () => {
    if (this._subscription) {
      this._unsubscribe();
    } else {
      this._subscribe();
    }


    

  };

  _subscribe = async () => {
    setUpdateIntervalForType(SensorTypes.magnetometer, 16);
    this._subscription = magnetometer.subscribe(
      sensorData => this.setState({magnetometer: this._angle(sensorData)}),
      error => console.log("The sensor is not available"),
    );
  };

  _unsubscribe = () => {
    this._subscription && this._subscription.unsubscribe();
    this._subscription = null;
  };


  _angle = magnetometer=> {
    let angle = 0;
    if (magnetometer) {
      let {x, y} = magnetometer;
      // if (Math.atan2(y, x) >= 0) {
      //   angle = Math.atan2(y, x) * (180 / Math.PI);
      // } else {
      //   angle = (Math.atan2(y, x) + 2 * Math.PI) * (180 / Math.PI);
      // }
       angle = Math.atan2(y, x) * (180/Math.PI);

    }
    return Math.round(LPF.next(angle));
  };

  _direction = degree => {

  };

  // Match the device top with pointer 0° degree. (By default 0° starts from the right of the device.)
  _degree = magnetometer => {
    XXX=magnetometer; 
  
if(st_button==0){
  YYY=magnetometer;
  magnetometer=C;
  }




else{

if(RoundO123==1){  
  if(i==0){
    st_AG='START ANGLE'
    magnetometer=magnetometer-YYY;  
    CC1=magnetometer;
    CC=magnetometer;
    CC=Math.abs(CC);
     C=magnetometer;
    }
    else if(i==1){
      st_AG='Left'
      magnetometer=magnetometer-YYY;  
      CCC1=magnetometer;
      CCC=magnetometer;
      CCC=Math.abs(CCC);
       C=magnetometer;
      }
      else if(i==2){
        st_AG='Right'
        magnetometer=magnetometer-YYY;  
        CCCC1=magnetometer;
        CCCC=magnetometer;
        CCCC=Math.abs(CCCC);
         C=magnetometer;
        }
      }

 else if(RoundO123==2){  
  if(i==0){
    magnetometer=magnetometer-YYY;  
    CC2=magnetometer;
    CC=magnetometer;
    CC=Math.abs(CC);
     C=magnetometer;
     st_AG='START ANGLE'
    }
    else if(i==1){
      magnetometer=magnetometer-YYY;  
      CCC2=magnetometer;
      CCC=magnetometer;
      CCC=Math.abs(CCC);
       C=magnetometer;
       st_AG='Left'
      }
      else if(i==2){
        magnetometer=magnetometer-YYY;  
        CCCC2=magnetometer;
        CCCC=magnetometer;
        CCCC=Math.abs(CCCC);
         C=magnetometer;
         st_AG='Right'
        }
      }

else if(RoundO123==3){  
  if(i==0){
    magnetometer=magnetometer-YYY;  
    CC3=magnetometer;
    CC=magnetometer;
    CC=Math.abs(CC);
     C=magnetometer;
     st_AG='START ANGLE'
    }
    else if(i==1){
      magnetometer=magnetometer-YYY;  
      CCC3=magnetometer;
      CCC=magnetometer;
      CCC=Math.abs(CCC);
       C=magnetometer;
       st_AG='Left'
      }
      else if(i==2){
        magnetometer=magnetometer-YYY;  
        CCCC3=magnetometer;
        CCCC=magnetometer;
        CCCC=Math.abs(CCCC);
         C=magnetometer;
         st_AG='Right'
        }
      } 
      



    }

    return 0-magnetometer;
  };

  render() {
    if(sent==true){
      this.storeUser22() ;
      sent=false;
   
    }
    return (      
      <Grid style={{backgroundColor: "white"}}>
        {/* <View style={{marginLeft:20,marginRight:'auto',marginTop:20}}>
            <Text style={{fontSize:18,fontWeight:'bold'}}>
                {RoundO123}
            </Text>
       </View> */}
        <Row style={{alignItems: "center"}} size={0.4}>

          <Col style={{alignItems: "center"}}>
            <Text
              style={{
                color: "#000000",
                fontSize: height / 26,
                fontWeight: "bold",
              }}
            >
              {this._degree(this.state.magnetometer)}°
            </Text>
          </Col>
        </Row>
 
        <Row style={{alignItems: "center"}} size={0.01}>
          <Col style={{alignItems: "center"}}>
            <View style={{width: width, alignItems: "center", bottom: 0}}>
              <Image
                source={require("../assets/compass_pointer.png")}
                style={{
                  height: height / 25,
                  resizeMode: "contain",
                }}
              />
            </View>
          </Col>
        </Row>
        
        <Row style={{alignItems: "center"}} size={1.4}>
          <Text
            style={{
              color: "#000000",
              fontSize: height / 27,
              width: width,
              position: "absolute",
              textAlign: "center",
            }}
          >
            {valueDG=this._degree(360-this.state.magnetometer)}°
          </Text>

          <Col style={{alignItems: "center"}}>
            <Image
              source={require("../assets/angleA.png")}
              style={{
                height: width - 100,
                justifyContent: "center",
                alignItems: "center",
                resizeMode: "contain",
                transform: [
                  {rotate:this._degree(this.state.magnetometer)+ "deg"}
                ],
              }}
            />
          </Col>
        </Row>

        <View style={{marginLeft:'auto',marginRight:'auto',marginTop:20}}>
            <Text style={{fontSize:16,fontWeight:'bold'}}>
                {st_AG}
            </Text>
       </View>


<View style={{marginRight:45,marginLeft:45,textAlign:'center'}}>
<DataTable>
      <DataTable.Header>
          <DataTable.Title>      Flextion</DataTable.Title>
          <DataTable.Title>           Start</DataTable.Title>
          <DataTable.Title >        Extention</DataTable.Title>
      </DataTable.Header>
      <DataTable.Row>
          <DataTable.Cell>        {CCC}</DataTable.Cell>
          <DataTable.Cell >          {CC}</DataTable.Cell>
          <DataTable.Cell >            {CCCC}</DataTable.Cell>
      </DataTable.Row> 
    </DataTable>
</View>


<View style={{ flexDirection: 'row',marginLeft:'auto', marginRight:'auto',marginTop:30}}>
</View>
<Row style={{alignItems: "center"}} size={1}>
  <Col style={{alignItems: "center"}} >
       
          <View style={[{ width: "70%", margintop: 10 }]}>
          <Button
           onPress={() => stbt() }
           color={colorBT}
           title= {stst}    
          />
        
        </View>  
        <View style={{ flexDirection: 'row',marginLeft:'auto', marginRight:'auto',marginTop:10}}>     
        <View style={{marginRight:5,width:132}}>
          <Button
           color="#737373"
           title= "Save"
           onPress={() => saveBT() }
          />
        </View> 
        <View style={{marginLeft:5,width:132}}>
          <Button
           color="#737373"
           title= "Tare"
           onPress={() => clearC() }
          />
        </View> 
</View>
</Col>
</Row>
</Grid>
    );
  }
}

const stbt = () => {
  if(st_button==1){
    stst="START";
    st_button=0;
    st_button2=0;
    colorBT="#5f9ea0"
    _picMS =valueDG;
    if(st_button2==0){
      st_button2=1;
    }
    else{
      st_button2=1;
    }
  }
  else{
    stst="STOP";
    st_button=1;
    st_button=1;
    colorBT="#fa8072"
    _picME =valueDG;
  } 
}

const styles =StyleSheet.create({
  container:{
      flex:1,
      padding:20
  },
  preloader:{
      position:'absolute',
      top:0,
      left:0,
      right:0,
      bottom:0,
      alignItems:'center',
      justifyContent:'center'
  }
})

const theme = {
  Button:{
      raised: true
  }
}

function saveBT(){
  C=0;
  i++;

if(i==3){
  RoundO123--;
  i=0;
CC='';
CCC='';
CCCC='';
st_AG='PREPARING';
}

if(RoundO123==0){
  i=0;
  RoundO123=cloneRound;
 if(cloneRound==1){
  AVG1=(Math.abs(CC1))/cloneRound;
  AVG2=(Math.abs(CCC1))/cloneRound;
  AVG3=(Math.abs(CCCC1))/cloneRound;
  Alert.alert(
    'Done',
    "No." +'     Start'+ '  '+ '        Left'+ '  '+'           Right'+"\n" 
    + "  1         " + Math.abs(CC1) +  '            '+ Math.abs(CCC1)+ '                  '+Math.abs(CCCC1)+"\n"  
    + "AVG:   "         +AVG1.toFixed(2)+  '      '+AVG2.toFixed(2)+ '               '+AVG3.toFixed(2),                               

    [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel"
      },
      { text:'OK', onPress: () =>  sent=true,}
    ]
  );
 } 


 else if(cloneRound==2){
  AVG1=(Math.abs(CC1)+Math.abs(CC2))/cloneRound;
  AVG2=(Math.abs(CCC1)+Math.abs(CCC2))/cloneRound;
  AVG3=(Math.abs(CCCC1)+Math.abs(CCCC2))/cloneRound;
  Alert.alert(
    'Done',
    "No." +'     Start'+ '  '+ '        Left'+ '  '+'           Right'+"\n" 
    + "  1         " + Math.abs(CC1) +  '            '+ Math.abs(CCC1)+ '                  '+Math.abs(CCCC1)+"\n"  
    + "  2         " + Math.abs(CC2) +  '            '+ Math.abs(CCC2)+ '                  '+Math.abs(CCCC2)+"\n" 
    + "AVG:   "       +AVG1.toFixed(1)+  '        '+AVG2.toFixed(2) + '             '+AVG3.toFixed(2),                              
    [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel"
      },
      { text: "OK",onPress: () =>sent=true}
    ]
  );
 }


 else if(cloneRound==3){
    AVG1=(Math.abs(CC1)+Math.abs(CC2)+ Math.abs(CC3))/cloneRound;
    AVG2=(Math.abs(CCC1)+Math.abs(CCC2)+ Math.abs(CCC3))/cloneRound;
    AVG3=(Math.abs(CCCC1)+Math.abs(CCCC2)+ Math.abs(CCCC3))/cloneRound;
  Alert.alert(
    'Done',
    "No." +'     Start'+ '  '+ '        Left'+ '  '+'           Right'+"\n" 
    + "  1         " + Math.abs(CC1) +  '            '+ Math.abs(CCC1)+ '                  '+Math.abs(CCCC1)+"\n"  
    + "  2         " + Math.abs(CC2) +  '            '+ Math.abs(CCC2)+ '                  '+Math.abs(CCCC2)+"\n" 
    + "  3         " + Math.abs(CC3) +  '            '+ Math.abs(CCC3)+ '                  '+Math.abs(CCCC3)+"\n"
    + "AVG:   "         +AVG1.toFixed(2)+  '      '+AVG2.toFixed(2) + '              '+AVG3.toFixed(2),                              
    [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel"
      },
      { text: "OK",onPress: () =>sent=true}
    ]
  );
 }
}
console.log(RoundO123);
}
function clearC(){
  C=0;
CC='';
CCC='';
CCCC='';
  
  
CC1='';
CCC1='';
CCCC1='';
  
CC2='';
 CCC2='';
CCCC2='';
  
CC3='';
CC3='';
CCCC3='';
i=0;
}