import React, {Component} from 'react'
import { Alert,StyleSheet,ScrollView,ActivityIndicator,View ,Text, ImageBackgroundBase} from 'react-native'
import { BackgroundImage } from 'react-native-elements/dist/config'

import  Icon  from 'react-native-vector-icons/FontAwesome'
import { ThemeProvider,Button, Input, Image } from 'react-native-elements'
import firestore from '@react-native-firebase/firestore';
var userID;


class Home2 extends Component{
    constructor(){
        super();
        this.dbRef = firestore();
        this.state = {
            name:"",
            password:"",
            Repass:"",
            isLoading: false
        }
    }

    inputValueUpdate = (val, prop)=>{
            const state =this.state;
            state[prop]=val;
            userID=state['name'];
            this.setState(state);
    }

    storeUser() {
        if(this.state.password==this.state.Repass){
        if(this.state.name == ''){
            alert('Fill at least your name!');
        }else{
            this.setState({
                isLoading:true
            })
            this.dbRef.collection(userID).add({
                name: this.state.name,
                password: this.state.password,
                Repass: this.state.Repass
            }).then((res)=>{
                this.setState({
                    name: '',
                    password: '',
                    Repass: '',
                    isLoading: false
                })
               
            })
            .catch((err)=>{
                console.log('Error :',err);
                this.setState({
                    isLoading: true
                })
            })
            
            Alert.alert(
            "Welcome",
            "Register already",
            [
              { text: "OK", onPress: () =>  this.props.navigation.navigate("Login") }
            ]
          );
        }
    }
    else{
        Alert.alert(
            "Error",
            "Password must match Retype Password",
            [
              { text: "OK", onPress: () =>  this.props.navigation.navigate("Home2") }
            ]
          ); 
    }
    }




render(){

if(this.state.isLoading){
        return(
            <View style={styles.preloader}>
                <ActivityIndicator size="large" color="#9E9E9E"/>
            </View>
        )
}

    return(
            <ThemeProvider theme={theme}>
                <ScrollView style={styles.container}>
                    <Image
                    source={{uri:'https://img.icons8.com/plasticine/2x/duolingo-logo.png'}}
                    style={{width:150,height:150}}
                    containerStyle={{marginLeft:'auto',marginRight:'auto',marginTop:60}}
                    />
                    <Input 
                        leftIcon={
                            <Icon
                                name='user-o'
                                size={20}
                                color='#20b2aa'
                            />
                        }
                        placeholder={' Username'}
                        value={this.state.name}
                        onChangeText={(val)=> this.inputValueUpdate(val,'name')}
                    />
                    
                    <Input secureTextEntry={true}
                        leftIcon={
                            <Icon
                                name='key'
                                size={20}
                                color='#20b2aa'
                            />
                        }
                        placeholder={' Password'}
                        value={this.state.password}
                        onChangeText={(val)=> this.inputValueUpdate(val,'password')}
                    />
                    <Input secureTextEntry={true}
                        leftIcon={
                            <Icon
                                name='key'
                                size={20}
                                color='#20b2aa'
                            />
                        }
                        placeholder={' Re-Password'}
                        value={this.state.Repass}
                        onChangeText={(val)=> this.inputValueUpdate(val,'Repass')}
                    />
                    <Button
                    icon={
                        <Icon 
                        name='check-square-o'
                        size={20}
                        color='white'
                        />
                    }
                        title=' Register'
                        
                        buttonStyle={{
                            backgroundColor: "#20b2aa"
                        }}
                    onPress={() => this.storeUser() }
                    />
                </ScrollView>   
            </ThemeProvider>
    )
}
}

const theme = {
    Button:{
        raised: true
    }
}

const styles =StyleSheet.create({
    container:{
        flex:1,
        padding:35
    },
    preloader:{
        position:'absolute',
        top:0,
        left:0,
        right:0,
        bottom:0,
        alignItems:'center',
        justifyContent:'center'
    }
})
export default Home2;























// import React, {Component} from 'react'
// import { StyleSheet,ScrollView,ActivityIndicator,View ,Text, ImageBackgroundBase} from 'react-native'
// import { BackgroundImage } from 'react-native-elements/dist/config'

// import  Icon  from 'react-native-vector-icons/FontAwesome'
// import { ThemeProvider,Button, Input, Image } from 'react-native-elements'
// import firestore from '@react-native-firebase/firestore';
// var userID;


// class Home2 extends Component{
//     constructor(){
//         super();

//         this.dbRef = firestore();
//         this.state = {
//             name:"",
//             password:"",
//             Repass:"",
//             isLoading: false
//         }
//     }

//     inputValueUpdate = (val, prop)=>{
//             const state =this.state;
//             state[prop]=val;
//             userID=state['name'];
//             this.setState(state);
//     }

//     storeUser() {
//         if(this.state.name == ''){
//             alert('Fill at least your name!');
//         }else{
//             this.setState({
//                 isLoading:true
//             })
//             this.dbRef.collection(userID).add({
//                 name: this.state.name,
//                 password: this.state.password,
//                 Repass: this.state.Repass
//             }).then((res)=>{
//                 this.setState({
//                     name: '',
//                     password: '',
//                     Repass: '',
//                     isLoading: false
//                 })
               
//             })
//             .catch((err)=>{
//                 console.log('Error :',err);
//                 this.setState({
//                     isLoading: true
//                 })
//             })
//         }
//     }




// render(){

// if(this.state.isLoading){
//         return(
//             <View style={styles.preloader}>
//                 <ActivityIndicator size="large" color="#9E9E9E"/>
//             </View>
//         )
// }

//     return(
//             <ThemeProvider theme={theme}>
//                 <ScrollView style={styles.container}>
//                     <Image
//                     source={{uri:'https://img.icons8.com/plasticine/2x/duolingo-logo.png'}}
//                     style={{width:150,height:150}}
//                     containerStyle={{marginLeft:'auto',marginRight:'auto'}}
//                     />
//                     <Input 
//                         leftIcon={
//                             <Icon
//                                 name='user-o'
//                                 size={20}
//                                 color='#f08080'
//                             />
//                         }
//                         placeholder={' Username'}
//                         value={this.state.name}
//                         onChangeText={(val)=> this.inputValueUpdate(val,'name')}
//                     />
                    
//                     <Input secureTextEntry={true}
//                         leftIcon={
//                             <Icon
//                                 name='key'
//                                 size={20}
//                                 color='#f08080'
//                             />
//                         }
//                         placeholder={' Password'}
//                         value={this.state.password}
//                         onChangeText={(val)=> this.inputValueUpdate(val,'password')}
//                     />
//                     <Input secureTextEntry={true}
//                         leftIcon={
//                             <Icon
//                                 name='key'
//                                 size={20}
//                                 color='#f08080'
//                             />
//                         }
//                         placeholder={' Re-Password'}
//                         value={this.state.Repass}
//                         onChangeText={(val)=> this.inputValueUpdate(val,'Repass')}
//                     />
//                     <Button
//                     icon={
//                         <Icon 
//                         name='check'
//                         size={20}
//                         color='white'
//                         />
//                     }
//                         title=' Add User'
                        
//                         buttonStyle={{
//                             backgroundColor: "#20b2aa"
//                         }}
//                     onPress={() => this.storeUser()}
//                     />
//                 </ScrollView>   
//             </ThemeProvider>
//     )
// }
// }

// const theme = {
//     Button:{
//         raised: true
//     }
// }

// const styles =StyleSheet.create({
//     container:{
//         flex:1,
//         padding:35
//     },
//     preloader:{
//         position:'absolute',
//         top:0,
//         left:0,
//         right:0,
//         bottom:0,
//         alignItems:'center',
//         justifyContent:'center'
//     }
// })
// export default Home2;