

import XLSX from 'xlsx';

import React, { Component,useState } from 'react';
import { PermissionsAndroid,StyleSheet, Text, View, Button, Alert, Image, ScrollView, TouchableWithoutFeedback,Platform} from 'react-native';
import { Table, Row, Rows, TableWrapper } from 'react-native-table-component';

import { Appbar, RadioButton,Divider } from 'react-native-paper';
import firestore from '@react-native-firebase/firestore';
import { ListItem,Badge} from 'react-native-elements'

// react-native-fs
import {readFile,writeFile, DocumentDirectoryPath,DownloadDirectoryPath } from 'react-native-fs';


const DDP = DownloadDirectoryPath + "/";

var start=[];
var flex=[];
var exten=[];
var start0=[];
var flex0=[];
var exten0=[];

var start1=[];
var flex1=[];
var exten1=[];
var start01=[];
var flex01=[];
var exten01=[];


var start2=[];
var flex2=[];
var exten2=[];
var start02=[];
var flex02=[];
var exten02=[];

var Astart=[];
var Aflex=[];
var Aexten=[];
var Astart0=[];
var Aflex0=[];
var Aexten0=[];

var LAS;
var II;


var picStrat=1;
var ii=[];

const input = res => res;
const output = str => str;
var nameUser;

export default class excelExport extends Component {
	constructor({props,route}) {
start=[];
flex=[];
exten=[];


start1=[];
flex1=[];
exten1=[];

start2=[];
flex2=[];
exten2=[];

Astart=[];
Aflex=[];
Aexten=[];


		super(props);
		this.dbRef = firestore();
		this.state = {isLoading: false,
            userArr:[],
			userArr2:[],
			userArr3:[],

			data: [["1","2","3"],
      [1,2,3],
      [1,2,3],
      [1,2,3],
      [1,2,3],
      [1,2,3],],

		};
		
		this.exportFile = this.exportFile.bind(this);

		const {itemId} = route.params;
        console.log(itemId);
        nameUser=itemId;
	};

	


    componentDidMount(){  
        console.log('this one call');
        this.unsubscribe = this.dbRef.collection(nameUser).orderBy("timestamp").onSnapshot(this.getCollection);
      }


	  getCollection=(querySnapshot)=>{
		const userArr=[];
		querySnapshot.forEach((res) => {
			const {    SF1,
				STF1,
				M1,
		
				SF2,
				STF2,
				M2,
		
				SF3,
				STF3,
				M3,
				
				SFA,
				STFA,
				MA,
				
				timeNeckFlex} =res.data();
			userArr.push({
				key: res.id,
				res,
			   
				SF1,
				STF1,
				M1,
		
				SF2,
				STF2,
				M2,
		
				SF3,
				STF3,
				M3,
				
				SFA,
				STFA,
				MA,
				
				timeNeckFlex
				
			})
			this.setState({
				userArr,
				isLoading:false
			})
		});
		calcuFlex();
	}


requestRunTimePermission=()=>{
  var that =this;
  async function externalStoragePermission(){
try{
  const granted = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
    {
      title :"External Storage Write Permission",
      message :"App needs access to Storage data.",
    }
  );
  if(granted === PermissionsAndroid.RESULTS.GRANTED){
    that.exportFile();
	start=[];
  }else{
    alert('WRITE_EXTERNAL_STORAGE permission denied');
  }
}catch (err){
  Alert,alert('Write permission err',err);
}
}

if(Platform.OS === 'android'){
  externalStoragePermission();
}else{
  this.exportFile();
}
}
exportFile() {
		var date = new Date().getDate(); //To get the Current Date
		var month = new Date().getMonth(); //To get the Current Month
		var year = new Date().getFullYear(); //To get the Current Year
		var hours = new Date().getHours(); //To get the Current Hours
		var min = new Date().getMinutes(); //To get the Current Minutes
		var sec = new Date().getSeconds(); //To get the Current Seconds
		var  Alltime=date+'|'+month+'|'+year;
	
		var data=[];
for (let index = 0; index < start.length; index++) {
			if(typeof(start[index]) !== 'undefined' && start[index] != null ||typeof(start1[index]) !== 'undefined' && start1[index] != null||typeof(start2[index]) !== 'undefined' && start2[index] != null||typeof(Astart[index]) !== 'undefined' && Astart[index] != null){            
				data.push(
			{"No":"1","Start":start[index],"Flextention":flex[index],"Extention":exten[index]},
			{"No":"2","Start":start1[index],"Flextention":flex1[index],"Extention":exten1[index]},
			{"No":"3","Start":start2[index],"Flextention":flex2[index],"Extention":exten2[index]},
			{"No":"avg","Start":Astart[index],"Flextention":Aflex[index],"Extention":Aexten[index]},
			{"":""},
			);
		}
}
		const ws = XLSX.utils.json_to_sheet(data);

		/* build new workbook */
		const wb = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(wb, ws, "SheetJS");

		/* write file */
		
		const G="FlexSheet"+Alltime+".xlsx";
		
		const wbout = XLSX.write(wb, {type:'binary', bookType:"xlsx"});
		const file = DDP + G ;

		writeFile(file, output(wbout), 'ascii').then((res) =>{
				Alert.alert("exportFile success", "Exported to " + file);
		}).catch((err) => { Alert.alert("exportFile Error", "Error " + err.message); });

	};
	




	render() { return (


<ScrollView>

				 {
				
                      this.state.userArr.map((item,i)=>{
                      
						
					    ii.push(picStrat);
						II=ii.length;
						LAS=ii.length-(i+1);

						start0.push(item.SF1);
						flex0.push(item.STF1);
						exten0.push(item.M1);

						start01.push(item.SF2);
						flex01.push(item.STF2);
						exten01.push(item.M2);

						start02.push(item.SF3);
						flex02.push(item.STF3);
						exten02.push(item.M3);

						Astart0.push(item.SFA);
						Aflex0.push(item.STFA);
						Aexten0.push(item.MA);

					
                       })
					   
                  }

<Text style={styles.welcome}>EXPORT DATA TO EXCEL FILE</Text>
<View style={{marginTop:10}}>
<Button onPress={()=>{this.requestRunTimePermission()}} 
            title="Export data" color="#008080" />
</View>
<Text>Flextention&Extention</Text>
</ScrollView>
	); };
};

const styles = StyleSheet.create({
	container: { flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#F5FCFF' },
	welcome: { fontSize: 20, textAlign: 'center', margin: 10 },
	instructions: { textAlign: 'center', color: '#333333', marginBottom: 5 },
	thead: { height: 40, backgroundColor: '#f1f8ff' },
	tr: { height: 30 },
	text: { marginLeft: 5 },
	table: { width: "100%" }
});

function calcuFlex(){
	for (let index = LAS; index < II; index++) {
		start.push(start0[index]);	
		flex.push(flex0[index]);
		exten.push(exten0[index]);
	}
	for (let index = LAS; index < II; index++) {
		start1.push(start01[index]);	
		flex1.push(flex01[index]);
		exten1.push(exten01[index]);
	}
	for (let index = LAS; index < II; index++) {
		start2.push(start02[index]);	
		flex2.push(flex02[index]);
		exten2.push(exten02[index]);
	}
	for (let index = LAS; index < II; index++) {
		Astart.push(Astart0[index]);	
		Aflex.push(Aflex0[index]);
		Aexten.push(Aexten0[index]);
	}
	console.log(start);
	console.log(flex);
	console.log(exten);	
}

