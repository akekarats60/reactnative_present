

import React from "react";
import { StyleSheet, Text, View,Button } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import HomeScreen from "./screens/HomeP";
import Home2 from "./screens/bpage";
import AccelerometerPage from "./screens/acc";
import Dashboard from "./screens/dashboard";
import MagnetometerPage from "./screens/meg";
import AllNeck from "./screens/AllNeck";

import FlexPage from "./screens/Flex";
import LetPage from "./screens/Let";
import RotPage from "./screens/Rot";

import Flexdata from "./screens/Flexdata";
import Letdata from "./screens/Letdata";
import Rotdata from "./screens/Rotdata";
import LetExcel from "./screens/letExcel";
import RotExcel from "./screens/Rotexcel";

import  Icon  from 'react-native-vector-icons/FontAwesome'

import excelExport from "./screens/excelExport";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { fonts } from "react-native-elements/dist/config";
import ArmPage from "./screens/Armdata";
import NeckPage from "./screens/Neckdata";

const Tab = createBottomTabNavigator();

// function HomeScreen({navigation}){
//   return(
//     <View>
//       <Text> HomeScreen </Text>
//       <Button title="Go to detail"
//       onPress={()=> navigation.navigate("Details") }/>
//     </View>
//   )
// }

// function DetailScreens(){
//   return(
//     <View>
//       <Text>  DetailScreens </Text>
//     </View>
//   )
// }

const Stack = createStackNavigator()


export default function App() { 
  return(
    // <View>
    //   <Text> testAppp </Text>
    // </View>
  <NavigationContainer >
    <Stack.Navigator initialRouteName="Login"  screenOptions={{
    headerShown: false
  }} >
        <Stack.Screen   name="Login" component={HomeScreen}/>
        
        <Stack.Screen name="Home2" component={Home2}/>
        <Stack.Screen name="AccelerometerPage" component={AccelerometerPage}/>
        <Stack.Screen name="MagnetometerPage" component={MagnetometerPage}/>
        <Stack.Screen name='AllNeck' component={AllNeck} />
        <Stack.Screen name="Dashboard" component={Dashboard}/> 
        <Stack.Screen name="ArmPage" component={ArmPage}/> 
        <Stack.Screen name="NeckPage" component={NeckPage}/>  

        <Stack.Screen name="RotPage" component={RotPage}/>  
        <Stack.Screen name="FlexPage" component={FlexPage}/>  
        <Stack.Screen name="LetPage" component={LetPage}/>  

        <Stack.Screen name="Flexdata" component={Flexdata}/>  
        <Stack.Screen name="Letdata" component={Letdata}/>  
        <Stack.Screen name="Rotdata" component={Rotdata}/>  
        <Stack.Screen name="excelExport" component={excelExport}/>  
        <Stack.Screen name="LetExcel" component={LetExcel}/> 
        <Stack.Screen name="RotExcel" component={RotExcel}/> 

    </Stack.Navigator>
  </NavigationContainer>


    // <NavigationContainer>
    // <Tab.Navigator initialRouteName="Login">
     
    //   <Tab.Screen name="function" component={Dashboard}/>
    //   <Tab.Screen name="Dashboard" component={Dashboard}/>
    //   <Tab.Screen name="Login" component={HomeScreen}/> 
      
    // </Tab.Navigator>

    // </NavigationContainer>
    )
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:"#fff",
    alignItems:"center",
    justifyContent:"center"
  }
})
























// 


// import React, {Component} from "react";
// import {Text, View, Image, Dimensions,StyleSheet,Button,Alert,SafeAreaView} from "react-native";
// import {Grid, Col, Row} from "react-native-easy-grid";
// import {magnetometer, SensorTypes, setUpdateIntervalForType} from "react-native-sensors";
// import LPF from "lpf";

// const {height, width} = Dimensions.get("window");
// var _picMS=0;
// var _picME=0;
// var st_button=false;
// var stst="START";
// var colorBT="#000080";
// var valueDG;
// var R;


// export default class App extends Component {
//   constructor() {
//     super();
//     this.state = {
//       magnetometer: "0",
//     };
//     LPF.init([]);
//     LPF.smoothing = 0.2;
//   }

//   componentDidMount() {
//     this._toggle();
//   }

//   componentWillUnmount() {
//     this._unsubscribe();
//   }

//   _toggle = () => {
//     if (this._subscription) {
//       this._unsubscribe();
//     } else {
//       this._subscribe();
//     }
//   };

//   _subscribe = async () => {
//     setUpdateIntervalForType(SensorTypes.magnetometer, 16);
//     this._subscription = magnetometer.subscribe(
//       sensorData => this.setState({magnetometer: this._angle(sensorData)}),
//       error => console.log("The sensor is not available"),
//     );
//   };

//   _unsubscribe = () => {
//     this._subscription && this._subscription.unsubscribe();
//     this._subscription = null;
//   };

//   _angle = magnetometer => {
//     let angle = 0;
//     if (magnetometer) {
//       let {x, y} = magnetometer;
//       if (Math.atan2(y, x) >= 0) {
//         angle = Math.atan2(y, x) * (180 / Math.PI);
//       } else {
//         angle = Math.atan2(y, x) * (180 / Math.PI);
//       }
//     }
//     return Math.round(LPF.next(angle));
//   };

//   _direction = degree => {

//   };

//   // Match the device top with pointer 0° degree. (By default 0° starts from the right of the device.)
//   _degree = magnetometer => {
//     return magnetometer - 90 >= 0
//       ? magnetometer - 90
//       : magnetometer + 271;
//   };

//   render() {
//     return (
//       <Grid style={{backgroundColor: "white"}}>
//         <Row style={{alignItems: "center"}} size={0.9}>
//           <Col style={{alignItems: "center"}}>
//             <Text
//               style={{
//                 color: "#fff",
//                 fontSize: height / 26,
//                 fontWeight: "bold",
//               }}
//             >
//               {this._direction(this._degree(this.state.magnetometer))}
//             </Text>
//           </Col>
//         </Row>

//         <Row style={{alignItems: "center"}} size={0.1}>
//           <Col style={{alignItems: "center"}}>
//             <View style={{width: width, alignItems: "center", bottom: 0}}>
//               <Image
//                 source={require("./assets/compass_pointer.png")}
//                 style={{
//                   height: height / 26,
//                   resizeMode: "contain",
//                 }}
//               />
//             </View>
//           </Col>
//         </Row>

//         <Row style={{alignItems: "center"}} size={2}>
//           <Text
//             style={{
//               color: "#fff",
//               fontSize: height / 27,
//               width: width,
//               position: "absolute",
//               textAlign: "center",
//             }}
//           >
//             {valueDG=this._degree(this.state.magnetometer)}°
//           </Text>

//           <Col style={{alignItems: "center"}}>
//             <Image
//               source={require("./assets/compass_bg.png")}
//               style={{
//                 height: width - 80,
//                 justifyContent: "center",
//                 alignItems: "center",
//                 resizeMode: "contain",
//                 transform: [
//                   {rotate: 360 - this.state.magnetometer + "deg"},
//                 ],
//               }}
//             />
//           </Col>
//         </Row>

//         <Text style={{
//           color: '#fff',
//            fontSize: height / 60,
//           // width: width,
//           // position: 'absolute',
//            textAlign: 'center'
//         }}>
//           Start  : {_picMS} °
//           </Text>

//           <Text style={{
//           color: '#fff',
//            fontSize: height / 60,
//           // width: width,
//           // position: 'absolute',
//            textAlign: 'center'
//         }}>
//           Stop  : {_picME} °
//           </Text>
//         <Text style={{
//           color: '#fff',
//            fontSize: height / 50,
//           // width: width,
//           // position: 'absolute',
//            textAlign: 'center'
//         }}>
//           Move  : {_picMS-_picME} °
//           </Text>



//         <Button
//         onPress={() => stbt() }
//         color={colorBT}
//         title= {stst}
//       />

//           <Row style={{alignItems: "center"}} size={1}>
//           <Col style={{alignItems: "center"}}>
//             <Text style={{color: "#fff"}}></Text>
//           </Col>
//         </Row>
//       </Grid>
//     );
//   }
// }

// const stbt = () => {
  
//   if(st_button==true){
//     stst="START";
//     st_button=false;
//     colorBT="#000080"
    
//     _picMS =valueDG;
//   }
//   else{
//     stst="STOP";
//     st_button=true;
//     colorBT="#ff0000"
//     _picME =valueDG;
//   }
 
// }